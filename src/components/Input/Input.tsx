import React, {ChangeEvent, useState} from 'react';
import InputMask from 'react-input-mask';
import styles from './Input.module.scss';
import clsx from "clsx";

interface InputProps {
    label: string;
    name: string;
    value: string;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
    type?: string;
    required?: boolean;
    mask?: string;
}

const Input: React.FC<InputProps> = ({ label, name, value, onChange, type = 'text', required = false, mask }) => {
    const [isInputFocused, setIsInputFocused] = useState<boolean>(false);
    const isInputFilled = value.trim() !== '';

    const handleFocus = () => {
        setIsInputFocused(true);
    };

    const handleBlur = () => {
        setIsInputFocused(false);
    };

    const renderInput = () => {
        const commonInputProps = {
            className: clsx(styles.input, { [styles.inputFocused]: isInputFocused }),
            type,
            name: name as string,
            value: value as string,
            onChange,
            onFocus: handleFocus,
            onBlur: handleBlur,
            required,
        };

        if (mask) {
            return (
                <InputMask mask={mask} maskPlaceholder="_" {...commonInputProps}>
                    <input />
                </InputMask>
            );
        } else {
            return <input {...commonInputProps} />;
        }
    };

    return (
        <div className={clsx(styles.inputWrapper, { [styles.inputWrapperFilled]: isInputFilled, [styles.inputWrapperFocused]: isInputFocused })}>
            <label className={styles.label}>
                {label}
            </label>
            {renderInput()}
        </div>
    );
};

export default Input;
