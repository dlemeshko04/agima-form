import React, { ChangeEvent, FormEvent, useState } from 'react';
import styles from './Form.module.scss';
import Input from '../Input/Input';
import Textarea from '../Textarea/Textarea';
import FileInput from '../FileInput/FileInput';
import Checkbox from '../Checkbox/Checkbox';
import Button from '../Button/Button';
import clsx from "clsx";
import List from "../List/List";

interface FormData {
    name: string;
    company: string;
    phone: string;
    email: string;
    message: string;
    file: File | null;
    agree: boolean;
}

const list = [
    {
        title: 'Чем вы занимаетесь?',
        sublist: [
            'Расскажите о своей компании.',
            'Как работаете, на чем зарабатываете?',
            'Кто ваши конкуренты?',
            'Чем вы от них отличаетесь?',
        ]
    },
    {
        title: 'В чем ваша задача?',
        sublist: [
            'Чего хотите достичь в ближайшем будущем?',
            'Что вам мешает?',
        ]
    },
    {
        title: 'Каким вы видите решение задачи?',
        sublist: [
            'Как планируете достичь своих целей?',
            'Какие решения пробовали раньше?',
        ]
    },
    {
        title: 'Какие у вас ожидания от результата?',
        sublist: [
            'В каком виде вы хотите видеть решение вашей задачи?',
            'В какой срок?',
            'Почему он важен? На что это должно быть похоже?',
        ]
    },
    {
        title: 'Сколько денег планируете потратить?',
        sublist: [
            'Каков ваш бюджет? ',
            'Почему вы готовы потратить именно такую сумму?',
        ]
    },
];

const Form: React.FC = () => {
    const [formData, setFormData] = useState<FormData>({
        name: '',
        company: '',
        email: '',
        phone: '',
        message: '',
        file: null,
        agree: false,
    });
    const [isFormFlipped, setIsFormFlipped] = useState<boolean>(false);
    const handleChange = (e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };
    const handleFileChange = (file: File | null) => {
        setFormData({
            ...formData,
            file: file || null,
        });
    };
    const handleCheckboxChange = () => {
        setFormData({
            ...formData,
            agree: !formData.agree,
        });
    };
    const handleSubmit = (e: FormEvent) => {
        e.preventDefault();
        console.log('Данные формы:', formData);
        alert('Отправлено');
        setFormData({
            name: '',
            company: '',
            email: '',
            phone: '',
            message: '',
            file: null,
            agree: false,
        });
    };

    return (
        <div className={styles.formWrapper}>
            <h1 className={styles.title}>
                Анкета для новых клиентов AGIMA
            </h1>
            <form onSubmit={handleSubmit} className={clsx(styles.form, { [styles.formFlipped]: isFormFlipped })}>
                <div className={styles.formFront}>
                    <p className={styles.formTitle}>
                        Сообщение в свободной форме
                    </p>
                    <div className={styles.formInputs}>
                        <Input
                            label="Ваше имя"
                            name="name"
                            value={formData.name}
                            onChange={handleChange}
                            required
                        />
                        <Input
                            label="Компания"
                            name="company"
                            value={formData.company}
                            onChange={handleChange}
                            required
                        />
                        <Input
                            label="Телефон"
                            name="phone"
                            value={formData.phone}
                            onChange={handleChange}
                            mask="+7 (999) 999-99-99"
                            required
                        />
                        <Input
                            label="Электронная почта"
                            name="email"
                            value={formData.email}
                            type="email"
                            onChange={handleChange}
                            required
                        />
                        <div className={styles.formInputsWrapper}>
                            <Textarea
                                label="Напишите текст обращения"
                                name="message"
                                value={formData.message}
                                onChange={handleChange}
                                required
                                disabled={!!formData.file}
                                onHintClick={() => setIsFormFlipped(!isFormFlipped)}
                            />
                            <FileInput
                                label="или прикрепите файл"
                                onChange={handleFileChange}
                                value={formData.file}
                                disabled={!!formData.message}
                            />
                        </div>
                    </div>
                    <div className={styles.formFooter}>
                        <Checkbox
                            name="agree"
                            checked={formData.agree}
                            onChange={handleCheckboxChange}
                            required
                        >
                    <span>
                        согласен на обработку моих <br/>
                        <a href="#">персональных данных</a>
                    </span>
                        </Checkbox>
                        <Button>
                            Отправить
                        </Button>
                    </div>
                </div>
                <div className={styles.formBack}>
                    <p className={styles.formBackTitle}>
                        Что написать в сообщении или файле?
                    </p>
                    <List items={list} />
                    <Button onClick={(e) => {e.preventDefault(); setIsFormFlipped(!isFormFlipped);}} type={'back'} className={styles.formBackBtn}>
                        Вернуться к заполнению
                    </Button>
                </div>
            </form>
        </div>
    );
};

export default Form;
