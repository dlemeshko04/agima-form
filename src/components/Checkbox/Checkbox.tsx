import React, {ChangeEvent, ReactNode} from 'react';
import styles from './Checkbox.module.scss';

interface CheckboxProps {
    name: string;
    checked: boolean;
    onChange: (e: ChangeEvent<HTMLInputElement>) => void;
    required?: boolean;
    children: ReactNode;
}

const Checkbox: React.FC<CheckboxProps> = ({ name, checked, onChange, required = false, children }) => {
    return (
        <label className={styles.checkboxWrapper}>
            <input
                type="checkbox"
                name={name}
                checked={checked}
                onChange={onChange}
                required={required}
                className={styles.input}
            />
            <span className={styles.checkbox}></span>
            {children}
        </label>
    );
};

export default Checkbox;
