import React, { ChangeEvent } from 'react';
import styles from './Textarea.module.scss';
import clsx from "clsx";

interface TextareaProps {
    label: string;
    name: string;
    value: string;
    onChange: (e: ChangeEvent<HTMLTextAreaElement>) => void;
    required?: boolean;
    disabled?: boolean;
    onHintClick: () => void;
}

const Textarea: React.FC<TextareaProps> = ({ label, name, value, onChange, required = false, disabled = false, onHintClick }) => {
    return (
        <div className={clsx(styles.textareaWrapper, { [styles.textareaWrapperDisabled]: disabled })}>
            <textarea
                name={name}
                value={value}
                onChange={onChange}
                required={required && !disabled}
                placeholder={label}
                className={styles.textarea}
            />
            <div className={styles.hint} onClick={onHintClick}>
                ?
            </div>
        </div>
    );
};

export default Textarea;
