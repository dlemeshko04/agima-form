import React, {ChangeEvent, MouseEvent} from 'react';
import styles from './FileInput.module.scss';
import clsx from 'clsx';

interface FileInputProps {
    label: string;
    onChange: (file: File | null) => void;
    disabled?: boolean;
    value: File | null;
}

const FileInput: React.FC<FileInputProps> = ({ label, onChange, disabled = false, value }) => {
    const handleFileChange = (e: ChangeEvent<HTMLInputElement>) => {
        const file = e.target.files?.[0] || null;
        onChange(file);
    };

    const handleClearClick = (event: MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        onChange(null);
    };

    return (
        <div className={clsx(styles.fileInput, { [styles.fileInputSelected]: value, [styles.fileInputDisabled]: disabled })}>
            <label className={styles.label}>
                <span>{label}</span>
                <input
                    type="file"
                    className={styles.input}
                    onChange={handleFileChange}
                />
            </label>
            {value && (
                <>
                    <span className={styles.fileName}>{value.name}</span>
                    <button className={styles.clearButton} onClick={handleClearClick}>
                        <svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <circle cx="8" cy="8.20001" r="8" fill="#C93131"/>
                            <rect x="3.05078" y="11.5355" width="12" height="2" transform="rotate(-45 3.05078 11.5355)" fill="white"/>
                            <rect x="4.46484" y="3.05023" width="12" height="2" transform="rotate(45 4.46484 3.05023)" fill="white"/>
                        </svg>
                    </button>
                </>
            )}
        </div>
    );
};

export default FileInput;
