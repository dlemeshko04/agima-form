import React from 'react';
import styles from './List.module.scss';

interface ListItem {
    title: string;
    sublist: string[];
}

interface ListProps {
    items: ListItem[];
}

const Form: React.FC<ListProps> = ({ items }) => {
    return (
        <div className={styles.list}>
            { items?.map((item: ListItem, idx: number) => (
                <div className={styles.listItem} key={idx}>
                    <p className={styles.listItemTitle}>
                        {`${idx + 1}. ${item.title}`}
                    </p>
                    { item.sublist?.map((sublistItem: string, sublistItemIdx: number) => (
                        <p className={styles.listItemText} key={sublistItemIdx}>
                            {sublistItem}
                        </p>
                    )) }
                </div>
            )) }
        </div>
    );
};

export default Form;
