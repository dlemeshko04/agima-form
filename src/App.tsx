import React from 'react';
import Form from './components/Form/Form';
import './styles/global.scss';
import "@fontsource/roboto";

function App() {
  return (
      <div className="App">
        <Form />
      </div>
  );
}

export default App;
